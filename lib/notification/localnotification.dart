import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:timezone/data/latest.dart' as ltz;

class LocalNotification {
  FlutterLocalNotificationsPlugin _flutterlocalnotification =
      FlutterLocalNotificationsPlugin();


  static Future initialiseNotification(
      ) async {
    FlutterLocalNotificationsPlugin flutterlocalnotification =
        FlutterLocalNotificationsPlugin();
    AndroidInitializationSettings androidInitializationSettings =
        const AndroidInitializationSettings("@mipmap/ic_launcher");

    InitializationSettings initializationSettings =
        InitializationSettings(android: androidInitializationSettings);
    ltz.initializeTimeZones();

    await flutterlocalnotification.initialize(initializationSettings);
  }

  Future sendNotification(String title, String body) async {
    AndroidNotificationDetails androidNotificationDetails =
        AndroidNotificationDetails('channelId', 'chinmoy',
            importance: Importance.max, priority: Priority.high);

    NotificationDetails notificationDetails =
        NotificationDetails(android: androidNotificationDetails);

    await _flutterlocalnotification.show(0, title, body, notificationDetails);
  }

 
}