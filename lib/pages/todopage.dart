import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:intl/intl.dart';
import 'package:notification_project/notification/localnotification.dart';

class TodoPage extends StatefulWidget {
  const TodoPage({super.key});

  @override
  State<TodoPage> createState() => _TodoPageState();
}

class _TodoPageState extends State<TodoPage> {


  LocalNotification _notificationService = LocalNotification();
  TextEditingController textEditingController = TextEditingController();
  TextEditingController deseditingcontroller = TextEditingController();





  @override
  void initState() {
    super.initState();
   
  }

  timeCount(String startTime, String endTime) {
    var format = DateFormat("HH:mm");
    var start = format.parse(startTime);
    var end = format.parse(endTime);

    if (start.isAfter(end)) {
      // var dtime = start.difference(end);
      return start.difference(end).inSeconds;
    } else {
      return end.difference(start).inSeconds;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        color: const Color.fromARGB(255, 215, 209, 216),
        height: 800,
        child: Padding(
            padding: const EdgeInsets.all(28.0),
            child:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
              TextFormField(
                decoration: const InputDecoration(
                  prefixIcon: Icon(Icons.title,color: Colors.blue,),
                  hintText: "Enter Title",
                  // hintStyle: TextStyle(color: Colors.white),
                  enabledBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 2, color: Colors.blue)),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(width: 2, color: Colors.blue),
                  ),
                ),
                controller: textEditingController,
              ),
              const SizedBox(
                height: 18,
              ),
              const SizedBox(
                height: 8,
              ),
              Container(
                height: 100,
                child: TextFormField(
                  maxLines: 50,
                  decoration: const InputDecoration(
                    hintText: "Description",
                    
                    enabledBorder: OutlineInputBorder(
                        borderSide: BorderSide(width: 2, color: Colors.blue)),
                    focusedBorder: OutlineInputBorder(
                      borderSide: BorderSide(width: 2, color: Colors.blue),
                    ),
                  ),
                  controller: deseditingcontroller,
                ),
              ),
              const SizedBox(
                height: 10,
              ),
              ElevatedButton(
                  style: ElevatedButton.styleFrom(
                    backgroundColor: Colors.blue,
                    padding: const EdgeInsets.symmetric(
                        horizontal: 50, vertical: 20),
                  ),
                  onPressed: () {
                    LocalNotification()
                        .sendNotification(textEditingController.text, deseditingcontroller.text);
                  },
                  child: const Text("Send Notification")),
              
            
const SizedBox(height: 10,),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  backgroundColor: Colors.blue,
                  padding:
                      const EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                ),
                onPressed: () async {
                  TimeOfDay? pickedTime = await showTimePicker(
                    initialTime: TimeOfDay.now(),
                    context: context,
                  );

                  if (pickedTime != null) {
                    //output 10:51 PM
                    DateTime parsedTime = DateFormat.jm()
                        // ignore: use_build_context_synchronously
                        .parse(pickedTime.format(context).toString());
                    String formattedTime = DateFormat('HH:mm:ss').format(parsedTime);
                    setState(() {
                      
                      Timer(
                          Duration(
                              seconds: timeCount(
                                      formattedTime,
                                      DateFormat('HH:mm:ss')
                                          .format(DateTime.now())
                                          .toString()) -
                                  30), () {
                        _notificationService.sendNotification(
                            textEditingController.text, deseditingcontroller.text);
                      });
                      
                    });
                  } else {
                  }
                },
                child: const Text("Set time"),
              ),

            ])),
      ),
    );
  }
}
